<?php
session_start();

require("../db/conexion.php");

$user = $_SESSION['user'];
if (!$user) {
  header('Location: /index.php');
}
?>

<a href="./logout.php">Logout</a>

<nav class="nav">
  <?php if ($user['rol'] === 'Administrador') { ?>
    <li class="nav-item">
      <a class="nav-link active" href="../view/index_registro.php">Users</a>
    </li>
  <?php } else { ?>
    <h1> Bienvenido <?php echo $user['nombre'];
                    echo " ";
                    echo $user['apellido'] ?> </h1>
  <?php } ?>
</nav>