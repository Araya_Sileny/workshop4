<?php
require('../db/conexion.php');


if ($_POST) {
  $username = $_POST['username'];
  $password = $_POST['password'];

  $user = authenticate($username, $password);

  if ($user) {
    session_start();
    $_SESSION['user'] = $user;

    header('Location: ../negocio/dashboard.php');
  } else {
    header('Location: ../view/index.php?status=login');
   
  }
}
