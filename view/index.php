<?php
session_start();
if ($_SESSION && $_SESSION['user']) {
  //user already logged in
  header('Location: ../negocio/dashboard.php');
}

$message = "";
if (!empty($_REQUEST['status'])) {
  switch ($_REQUEST['status']) {
    case 'login':
      $message = 'User does not exists';
      break;
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ESHOP CR</title>
  <link rel="stylesheet" href="../css/principal.css">
  <!--JQUERY-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

  <!-- Los iconos tipo Solid de Fontawesome-->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
  <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
</head>

<body>
  <div class="modal-dialog text-center">
    <div class="col-sm-8 main-section">
      <div class="modal-content">
        <div class="msg">
          <?php echo $message; ?>
        </div>
        <form class="col-12" method="POST" action="../negocio/login.php">
          <h4 class="title">Inicio de Sesion</h4>
          <div class="form-group" id="user-group">
            <input type="text" class="form-control" placeholder="Nombre de usuario" name="username" />
          </div>
          <div class="form-group" id="contrasena-group">
            <input type="password" class="form-control" placeholder="Contraseña" name="password" />
          </div>
          <button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Ingresar </button><br>
          <br>
          <div class="col-12 forgot">
            <br>
            <br>
          </div>
        </form>
      </div>
    </div>
  </div>
</body>

</html>