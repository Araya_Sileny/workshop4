<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
    <link rel = "stylesheet" href="../css/principal.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <!-- Los iconos tipo Solid de Fontawesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
    <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
</head>
<body>
    <div class="modal-dialog text-center">
        <div class="col-sm-8 main-section">
            <div class="modal-content">
                <form method="post" name="form" action="../view/registrarCliente.php">
                    <h4 class= "title">Registro Cliente</h4>
                    <div class="form-group" id="user-group">
                        <input type="text"  name= "usuario" placeholder= "Usuario">
                    </div>
                    <div class="form-group" id="contrasena-group">
                        <input type="password" name= "contraseña" placeholder= "Contraseña">
                    </div>
                    <div class="form-group" id="name-group">
                        <input type="text" name= "nombre" placeholder= "Nombre">
                    </div>
                    <div class="form-group" id="lastname-group">
                        <input type="text" name= "apellido" placeholder= "Apellido">
                    </div>
                    <input type="submit" class="btn btn-outline-primary" name="send" value="Agregar">
                    <br>
                    <br>
                </form>
            </div> 
        </div>
    </div>
</body>
</html>